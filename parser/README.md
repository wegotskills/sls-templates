# Serverless Parser Template

This repository provides base `sls` template for a skillset parser.

### Creating a new project

```
sls create --template-url https://bitbucket.org/wegotskills/sls-templates/src/master/parser/ -n my-new-parser
```